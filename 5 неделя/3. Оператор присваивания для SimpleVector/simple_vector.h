#pragma once

#include <cstdlib>
#include <algorithm>

// Реализуйте шаблон SimpleVector
template<typename T>
class SimpleVector
{
public:
    SimpleVector();

    explicit SimpleVector(size_t size);

    SimpleVector(const SimpleVector& rhv);

    ~SimpleVector();

    T& operator[](size_t index);

    SimpleVector& operator=(const SimpleVector& rhv);

    T* begin();

    T* end();

    const T* begin() const;

    const T* end() const;

    size_t Size() const;

    size_t Capacity() const;

    void PushBack(const T& value);

private:
    void resize();

    void resize(size_t newCapacity);

    T* _items;
    size_t _size, _capacity;
    // Добавьте поля для хранения данных вектора
};

template<typename T>
SimpleVector<T>::SimpleVector() : _items(nullptr), _size(0), _capacity(0)
{
}

template<typename T>
SimpleVector<T>::SimpleVector(size_t size) : _items(new T[size]), _size(size), _capacity(size)
{
}

template<typename T>
SimpleVector<T>::SimpleVector(const SimpleVector<T>& rhv) : _items(new T[rhv._capacity]), _size(rhv._size),
                                                            _capacity(rhv._capacity)
{
    std::copy(rhv.begin(), rhv.end(), begin());
}

template<typename T>
SimpleVector<T>::~SimpleVector()
{
    if (_items)
        delete[] _items;
}

template<typename T>
T& SimpleVector<T>::operator[](size_t index)
{
    return _items[index];
}

template<typename T>
T* SimpleVector<T>::begin()
{
    return _items;
}

template<typename T>
T* SimpleVector<T>::end()
{
    return _items + _size;
}

template<typename T>
const T* SimpleVector<T>::begin() const
{
    return _items;
}

template<typename T>
const T* SimpleVector<T>::end() const
{
    return _items + _size;
}

template<typename T>
size_t SimpleVector<T>::Size() const
{
    return _size;
}

template<typename T>
size_t SimpleVector<T>::Capacity() const
{
    return _capacity;
}

template<typename T>
void SimpleVector<T>::resize()
{
    T* temp = new T[_capacity * 2];
    for (size_t i = 0; i < _size; ++i)
        temp[i] = _items[i];
    delete[] _items;
    _items = temp;
    _capacity *= 2;
}

template<typename T>
void SimpleVector<T>::PushBack(const T& value)
{
    if (_capacity == 0)
        resize(1);
    else if (_size == _capacity)
        resize();
    _items[_size] = value;
    ++_size;
}

template<typename T>
void SimpleVector<T>::resize(size_t newCapacity)
{
    T* temp = new T[newCapacity];
    for (size_t i = 0; i < _size && i < newCapacity; ++i)
        temp[i] = _items[i];
    delete[] _items;
    _items = temp;
    _capacity = newCapacity;
}

template<typename T>
SimpleVector<T>& SimpleVector<T>::operator=(const SimpleVector<T>& rhv)
{
    if (rhv._size <= _capacity)
    {
        std::copy(rhv.begin(), rhv.end(), begin());
        _size = rhv._size;
    } else
    {
        SimpleVector<T> tmp(rhv);
        std::swap(tmp._items, _items);
        std::swap(tmp._size, _size);
        std::swap(tmp._capacity, _capacity);
    }
    return *this;
}
