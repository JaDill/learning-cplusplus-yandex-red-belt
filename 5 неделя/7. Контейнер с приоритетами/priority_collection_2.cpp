#include "test_runner.h"
#include <algorithm>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <set>
#include <utility>
#include <vector>
#include <list>
#include <unordered_map>

using namespace std;

template<typename T>
class PriorityCollection
{
public:
    using Id = typename list<T>::iterator;
    using OuterId = typename set<tuple<int, int, Id>>::iterator;

    // Добавить объект с нулевым приоритетом
    // с помощью перемещения и вернуть его идентификатор
    Id Add(T object);

    // Добавить все элементы диапазона [range_begin, range_end)
    // с помощью перемещения, записав выданные им идентификаторы
    // в диапазон [ids_begin, ...)
    template<typename ObjInputIt, typename IdOutputIt>
    void Add(ObjInputIt range_begin, ObjInputIt range_end,
             IdOutputIt ids_begin);

    // Определить, принадлежит ли идентификатор какому-либо
    // хранящемуся в контейнере объекту
    bool IsValid(Id id) const;

    // Получить объект по идентификатору
    const T& Get(Id id) const;

    // Увеличить приоритет объекта на 1
    void Promote(Id id);

    // Получить объект с максимальным приоритетом и его приоритет
    pair<const T&, int> GetMax() const;

    // Аналогично GetMax, но удаляет элемент из контейнера
    pair<T, int> PopMax();

private:

    struct Less
    {
        bool operator()(const tuple<int, int, Id>& lhv, const tuple<int, int, Id>& rhv)
        {
            if (get<0>(lhv) == get<0>(rhv))
                return get<1>(lhv) < get<1>(rhv);
            return get<0>(lhv) < get<0>(rhv);
        }
    };

    struct Hash
    {
        size_t operator()(const Id& key) const
        {
            return reinterpret_cast<std::uintptr_t>(key._M_node);
        }
    };

    set<tuple<int/*priority*/, int/*position*/, Id>, Less> _priorityItems;
    unordered_map<Id, OuterId, Hash> _fastSearch;
    list<T> _items;
    int positionCounter = 0;
};

template<typename T>
typename PriorityCollection<T>::Id PriorityCollection<T>::Add(T object)
{
    auto innerIterator = _items.insert(_items.end(), move(object));
    auto setIterator = _priorityItems.insert({0, positionCounter++, innerIterator}).first;
    _fastSearch[innerIterator] = setIterator;
    return innerIterator;
}

template<typename T>
template<typename ObjInputIt, typename IdOutputIt>
void PriorityCollection<T>::Add(ObjInputIt range_begin, ObjInputIt range_end, IdOutputIt ids_begin)
{
    for (auto it = range_begin; it != range_end; ++it)
    {
        *ids_begin++ = Add(move(*it));
    }
}

template<typename T>
bool PriorityCollection<T>::IsValid(PriorityCollection::Id id) const
{
    return _fastSearch.find(id) != _fastSearch.end();
}

template<typename T>
const T& PriorityCollection<T>::Get(PriorityCollection::Id id) const
{
    return *id;
}

template<typename T>
void PriorityCollection<T>::Promote(PriorityCollection::Id id)
{
    auto mapIterator = _fastSearch.find(id);
    auto[priority, position, object] = *mapIterator->second;
    _priorityItems.erase(*mapIterator->second);
    _fastSearch[id] = _priorityItems.insert({priority + 1, position, id}).first;
}

template<typename T>
pair<const T&, int> PriorityCollection<T>::GetMax() const
{
    auto[priority, position, object] = *prev(_priorityItems.end());
    return {*(object), priority};
}

template<typename T>
pair<T, int> PriorityCollection<T>::PopMax()
{
    auto priorityIterator = prev(_priorityItems.end());
    auto[priority, position, object] = *prev(_priorityItems.end());
    _fastSearch.erase(object);
    T realObject = move(*object);
    _priorityItems.erase(priorityIterator);
    return {move(realObject), priority};
}

class StringNonCopyable : public string
{
public:
    using string::string;  // Позволяет использовать конструкторы строки
    StringNonCopyable(const StringNonCopyable&) = delete;

    StringNonCopyable(StringNonCopyable&&) = default;

    StringNonCopyable& operator=(const StringNonCopyable&) = delete;

    StringNonCopyable& operator=(StringNonCopyable&&) = default;
};

void TestNoCopy()
{
    PriorityCollection<StringNonCopyable> strings;
    const auto white_id = strings.Add("white");
    const auto yellow_id = strings.Add("yellow");
    const auto red_id = strings.Add("red");

    ASSERT(strings.IsValid(white_id));
    ASSERT(strings.IsValid(yellow_id));
    ASSERT(strings.IsValid(red_id));

    for (int i = 0; i < 2; ++i)
    {
        strings.Promote(yellow_id);
        strings.Promote(red_id);
    }

    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "red");
        ASSERT_EQUAL(item.second, 2);
    }
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "yellow");
        ASSERT_EQUAL(item.second, 2);
    }
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "white");
        ASSERT_EQUAL(item.second, 0);
    }

    ASSERT(!strings.IsValid(white_id));
    ASSERT(!strings.IsValid(yellow_id));
    ASSERT(!strings.IsValid(red_id));
}

void mine()
{
    PriorityCollection<StringNonCopyable> strings;
    const auto id_1 = strings.Add("white");
    const auto id_2 = strings.Add("yellow");
    ASSERT(strings.IsValid(id_1));
    ASSERT(strings.IsValid(id_2));
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "yellow");
        ASSERT_EQUAL(item.second, 0);
    }
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "white");
        ASSERT_EQUAL(item.second, 0);
    }
    ASSERT(!strings.IsValid(id_1));
    ASSERT(!strings.IsValid(id_2));
    const auto id_3 = strings.Add("red");
    const auto id_4 = strings.Add("blue");
    ASSERT(strings.IsValid(id_3));
    ASSERT(strings.IsValid(id_4));
    for (int i = 0; i < 10; ++i)
    {
        strings.Promote(id_3);
    }
    for (int i = 0; i < 9; ++i)
    {
        strings.Promote(id_4);
    }
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "red");
        ASSERT_EQUAL(item.second, 10);
    }
    {
        const auto item = strings.PopMax();
        ASSERT_EQUAL(item.first, "blue");
        ASSERT_EQUAL(item.second, 9);
    }
    ASSERT(!strings.IsValid(id_3));
    ASSERT(!strings.IsValid(id_4));
}

void totalMine()
{
    PriorityCollection<StringNonCopyable> strings;
    vector<StringNonCopyable> data;
    StringNonCopyable one("one"), two("two"), three("three");
    data.push_back(move(one));
    data.push_back(move(two));
    data.push_back(move(three));
    vector<PriorityCollection<StringNonCopyable>::Id> iters;
    strings.Add(data.begin(), data.end(), back_inserter(iters));
    for (const auto& item : iters)
    {
        ASSERT(strings.IsValid(item));
        strings.Promote(item);
    }
    ASSERT_EQUAL(strings.Get(iters[0]), StringNonCopyable{"one"});
    ASSERT_EQUAL(strings.Get(iters[1]), StringNonCopyable{"two"});
    ASSERT_EQUAL(strings.Get(iters[2]), StringNonCopyable{"three"});
    {
        const auto preitem = strings.GetMax();
        const auto item = strings.PopMax();
        ASSERT_EQUAL(preitem.second, item.second);
        ASSERT_EQUAL(item.first, "three");
        ASSERT_EQUAL(item.second, 1);
    }
    strings.Promote(iters[0]);
    {
        const auto preitem = strings.GetMax();
        const auto item = strings.PopMax();
        ASSERT_EQUAL(preitem.second, item.second);
        ASSERT_EQUAL(item.first, "one");
        ASSERT_EQUAL(item.second, 2);
    }
    {
        const auto preitem = strings.GetMax();
        const auto item = strings.PopMax();
        ASSERT_EQUAL(preitem.second, item.second);
        ASSERT_EQUAL(item.first, "two");
        ASSERT_EQUAL(item.second, 1);
    }
}

int main()
{
    TestRunner tr;
    RUN_TEST(tr, TestNoCopy);
    RUN_TEST(tr, mine);
    RUN_TEST(tr, totalMine);
    return 0;
}