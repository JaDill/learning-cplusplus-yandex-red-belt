#include "test_runner.h"

#include <vector>

using namespace std;

// ��������� Sentence<Token> ��� ������������� ���� Token
// ��������� vector<Token>.
// ��������� ����� � �������� ������������� ��������
// ������� ����� ������� �� ������������ ������ ��������,
// � ������ ����������� � vector<Sentence<Token>>.
template <typename Token>
using Sentence = vector<Token>;

// ����� Token ����� ����� bool IsEndSentencePunctuation() const
template <typename Token>
vector<Sentence<Token>> SplitIntoSentences( vector<Token> tokens )
{
	vector<Sentence<Token>> sentenceList;
	Sentence<Token> sentence;
	auto it = begin( tokens );
	while(it != end( tokens ))
	{
		if(it->IsEndSentencePunctuation())
		{
			do
			{
				sentence.push_back( move( *it++ ) );
			} while(it != end( tokens ) && it->IsEndSentencePunctuation());

			sentenceList.push_back( move( sentence ) );
		}
		else
		{
			sentence.push_back( move( *it++ ) );
		}
	}
	if(!sentence.empty())
	{
		sentenceList.push_back( move( sentence ) );
	}
	return move( sentenceList );
}


struct TestToken {
	string data;
	bool is_end_sentence_punctuation = false;

	bool IsEndSentencePunctuation() const {
		return is_end_sentence_punctuation;
	}
	bool operator==( const TestToken& other ) const {
		return data == other.data && is_end_sentence_punctuation == other.is_end_sentence_punctuation;
	}
};

ostream& operator<<( ostream& stream, const TestToken& token ) {
	return stream << token.data;
}

// ���� �������� ����������� �������� ������ TestToken.
// ��� �������� ���������� ����������� � ������� SplitIntoSentences
// ���������� �������� ��������� ����.
void TestSplitting() {
	ASSERT_EQUAL(
		SplitIntoSentences( vector<TestToken>( { {"Split"}, {"into"}, {"sentences"}, {"!"} } ) ),
		vector<Sentence<TestToken>>( {
			{{"Split"}, {"into"}, {"sentences"}, {"!"}}
			} )
	);

	ASSERT_EQUAL(
		SplitIntoSentences( vector<TestToken>( { {"Split"}, {"into"}, {"sentences"}, {"!", true} } ) ),
		vector<Sentence<TestToken>>( {
			{{"Split"}, {"into"}, {"sentences"}, {"!", true}}
			} )
	);

	ASSERT_EQUAL(
		SplitIntoSentences( vector<TestToken>( { {"Split"}, {"into"}, {"sentences"}, {"!", true}, {"!", true}, {"Without"}, {"copies"}, {".", true} } ) ),
		vector<Sentence<TestToken>>( {
			{{"Split"}, {"into"}, {"sentences"}, {"!", true}, {"!", true}},
			{{"Without"}, {"copies"}, {".", true}},
			} )
			);
}

int main() {
	TestRunner tr;
	RUN_TEST( tr, TestSplitting );
	return 0;
}