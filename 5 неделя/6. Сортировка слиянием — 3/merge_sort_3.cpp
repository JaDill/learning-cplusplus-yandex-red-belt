#include "test_runner.h"
#include <algorithm>
#include <memory>
#include <vector>

using namespace std;

template<typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    size_t size = distance(range_begin, range_end);
    if (size < 2)
        return;
    vector<typename RandomIt::value_type> everything(make_move_iterator(range_begin), make_move_iterator(range_end));
    auto end_1 = everything.begin() + size / 3, end_2 = everything.begin() + size / 3 * 2;
    MergeSort(everything.begin(), end_1);
    MergeSort(end_1, end_2);
    MergeSort(end_2, everything.end());
    vector<typename RandomIt::value_type> preresult;
    merge(make_move_iterator(everything.begin()), make_move_iterator(end_1), make_move_iterator(end_1),
          make_move_iterator(end_2), back_inserter(preresult));
    merge(make_move_iterator(preresult.begin()), make_move_iterator(preresult.end()), make_move_iterator(end_2),
          make_move_iterator(everything.end()), range_begin);
}

void TestIntVector()
{
    vector<int> numbers = {6, 1, 3, 9, 1, 9, 8, 12, 1};
    MergeSort(begin(numbers), end(numbers));
    ASSERT(is_sorted(begin(numbers), end(numbers)));
}

int main()
{
    TestRunner tr;
    RUN_TEST(tr, TestIntVector);
    return 0;
}