#include "test_runner.h"
#include "profile.h"

#include <map>
#include <string>
#include <set>
#include <future>
#include <functional>

using namespace std;

template<typename Iterator>
class IteratorRange
{
public:
    IteratorRange(Iterator begin, Iterator end);

public:
    Iterator begin() const;

    Iterator end() const;

    size_t size() const;

private:
    Iterator _begin;
    Iterator _end;
    size_t _size;
};

template<typename Iterator>
IteratorRange<Iterator>::IteratorRange(Iterator begin, Iterator end) : _begin(begin), _end(end),
                                                                       _size(distance(begin, end)) {}

template<typename Iterator>
Iterator IteratorRange<Iterator>::begin() const
{
    return _begin;
}

template<typename Iterator>
Iterator IteratorRange<Iterator>::end() const
{
    return _end;
}

template<typename Iterator>
size_t IteratorRange<Iterator>::size() const
{
    return _size;
}

template<typename Iterator>
class Paginator
{
public:
    Paginator(Iterator begin, Iterator end, size_t pageSize);

public:
    auto begin() const;

    auto end() const;

    size_t size() const;

private:
    vector<IteratorRange<Iterator>> items;
};

template<typename Iterator>
Paginator<Iterator>::Paginator(Iterator begin, Iterator end, size_t pageSize)
{
    Iterator top = begin;
    size_t leftPages = distance(begin, end);
    while (leftPages > 0)
    {
        items.push_back({top, next(top, min(leftPages, pageSize))});
        top = items.back().end();
        leftPages -= min(leftPages, pageSize);
    }
}

template<typename Iterator>
auto Paginator<Iterator>::begin() const
{
    return items.begin();
}

template<typename Iterator>
auto Paginator<Iterator>::end() const
{
    return items.end();
}

template<typename Iterator>
size_t Paginator<Iterator>::size() const
{
    return items.size();
}

template<typename C>
auto Paginate(C& c, size_t page_size)
{
    return Paginator(begin(c), end(c), page_size);
}


struct Stats
{
    map<string, int> word_frequences;

    void operator+=(const Stats& other)
    {
        for (const auto&[key, value] : other.word_frequences)
            this->word_frequences[key] += value;
    }
};

Stats ExploreLine(const set<string>& key_words, const string& line)
{
    Stats result;
    size_t i = 0, j = 0;
    string temp;
    while (true)
    {
        temp.erase();
        while (i < line.size() && line[i] == ' ')
            ++i;
        if (i >= line.size())
            break;
        temp.push_back(line[i]);
        j = i + 1;
        while (j < line.size() && line[j] != ' ')
        {
            temp.push_back(line[j]);
            ++j;
        }
        if (j <= line.size())
        {
            if (key_words.find(temp) != key_words.end())
                ++result.word_frequences[temp];
            i = j + 1;
        }
    }
    return result;
}

Stats ExploreKeyWordsSingleThread(
        const set<string>& key_words, IteratorRange<vector<string>::iterator> range, const vector<string>& input
)
{
    Stats result;
    for (const auto& it : range)
    {
        result += ExploreLine(key_words, it);
    }
    return result;
}

Stats ExploreKeyWords(const set<string>& key_words, istream& input)
{
    vector<string> inputVector;
    for (string line; getline(input, line);)
    {
        inputVector.push_back(line);
    }
    Stats result;
    {
        LogDuration name("1");

        vector<future<Stats>> futures;
        vector<Stats> results;
        for (auto page : Paginate(inputVector, 5000))
        {
            futures.push_back(async(ExploreKeyWordsSingleThread, ref(key_words), page, ref(inputVector)));
        }
        for (auto& f : futures)
            results.push_back(f.get());
        for (const auto& r : results)
            result += r;
    }
    return result;
}

void TestBasic()
{
    const set<string> key_words = {"yangle", "rocks", "sucks", "all"};

    stringstream ss;
    const int N = 10'000;
    for (int i = 0; i < N; ++i)
    {
        ss << "this new yangle service really rocks\n";
        ss << "It sucks when yangle isn't available\n";
        ss << "10 reasons why yangle is the best IT company\n";
        ss << "yangle rocks others suck\n";
        ss << "Goondex really sucks, but yangle rocks. Use yangle\n";
    }


    const auto stats = ExploreKeyWords(key_words, ss);
    const map<string, int> expected = {
            {"yangle", 6 * N},
            {"rocks",  2 * N},
            {"sucks",  1 * N}
    };
    ASSERT_EQUAL(stats.word_frequences, expected);
}

int main()
{
    TestRunner tr;
    RUN_TEST(tr, TestBasic);
}