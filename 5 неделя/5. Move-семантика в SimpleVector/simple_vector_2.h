#include <cstdint>
#include <algorithm>
#include <cstdlib>
#include <stdexcept>
#include <utility>

// Реализуйте SimpleVector в этом файле
// и отправьте его на проверку

template<typename T>
class SimpleVector
{
public:
    SimpleVector();

    explicit SimpleVector(size_t size);

    SimpleVector(const SimpleVector& rhv) = delete;

    SimpleVector(SimpleVector&& rhv);

    ~SimpleVector();

    T& operator[](size_t index);

    SimpleVector& operator=(const SimpleVector& rhv) = delete;

    SimpleVector& operator=(SimpleVector&& rhv);

    T* begin();

    T* end();

    const T* begin() const;

    const T* end() const;

    size_t Size() const;

    size_t Capacity() const;

    void PushBack(const T& value);

    void PushBack(T&& value);

private:
    void resize(size_t newCapacity);

private:
    T* _items;
    size_t _size, _capacity;
};

template<typename T>
SimpleVector<T>::SimpleVector(size_t size) : _items(new T[size]), _size(size), _capacity(size)
{
}

template<typename T>
SimpleVector<T>::~SimpleVector()
{
    if (_items)
        delete[] _items;
}

template<typename T>
T& SimpleVector<T>::operator[](size_t index)
{
    if (index >= _size)
        throw std::out_of_range("index cannot be great than size");
    return _items[index];
}

template<typename T>
SimpleVector<T>::SimpleVector() : _items(nullptr), _size(0), _capacity(0)
{
}

template<typename T>
T* SimpleVector<T>::begin()
{
    return _items;
}

template<typename T>
T* SimpleVector<T>::end()
{
    return _items + _size;
}

template<typename T>
const T* SimpleVector<T>::begin() const
{
    return _items;
}

template<typename T>
const T* SimpleVector<T>::end() const
{
    return _items + _size;
}

template<typename T>
size_t SimpleVector<T>::Size() const
{
    return _size;
}

template<typename T>
size_t SimpleVector<T>::Capacity() const
{
    return _capacity;
}

template<typename T>
void SimpleVector<T>::PushBack(const T& value)
{
    if (_size + 1 > _capacity)
        resize((_size + 1) * 2);
    _items[_size] = value;
    ++_size;
}

template<typename T>
void SimpleVector<T>::PushBack(T&& value)
{
    if (_size + 1 > _capacity)
        resize((_size + 1) * 2);
    _items[_size] = std::move(value);
    ++_size;
}

template<typename T>
void SimpleVector<T>::resize(size_t newCapacity)
{
    T* newItems = new T[newCapacity];
    std::move(begin(), end(), newItems);
    delete[] _items;
    _items = newItems;
    _capacity = newCapacity;
}

template<typename T>
SimpleVector<T>::SimpleVector(SimpleVector&& rhv) : _items(std::move(rhv._items)), _size(rhv._size), _capacity(rhv._capacity)
{
}

template<typename T>
SimpleVector<T>& SimpleVector<T>::operator=(SimpleVector&& rhv)
{
    delete[] _items;
    SimpleVector<T> temp(rhv);
    _items = std::move(temp._items);
    _size = rhv._size;
    _capacity = rhv._capacity;
    return *this;
}
