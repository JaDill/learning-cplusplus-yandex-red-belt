#include <vector>

using namespace std;

template<typename T>
class LinkedList
{
public:
    struct Node
    {
        T value;
        Node* next = nullptr;
    };

    ~LinkedList();

    void PushFront(const T& value);

    void InsertAfter(Node* node, const T& value);

    void RemoveAfter(Node* node);

    void PopFront();

    Node* GetHead()
    { return head; }

    const Node* GetHead() const
    { return head; }

private:
    Node* head = nullptr;
};

template<typename T>
LinkedList<T>::~LinkedList()
{
    while (head) {
        Node* temp = head->next;
        delete head;
        head = temp;
    }
}

template<typename T>
void LinkedList<T>::PushFront(const T& value)
{
    Node* newNode = new Node;
    newNode->value = value;
    newNode->next = head;
    head = newNode;
}

template<typename T>
void LinkedList<T>::InsertAfter(LinkedList::Node* node, const T& value)
{
    if(!node){
        PushFront(value);
        return;
    }
    Node* newNode = new Node;
    newNode->value = value;
    newNode->next = node->next;
    node->next = newNode;
}

template<typename T>
void LinkedList<T>::RemoveAfter(LinkedList::Node* node)
{
    if(!node){
        PopFront();
        return;
    }
    if (!node->next)
        return;
    Node* toRemove = node->next;
    node->next = toRemove->next;
    delete toRemove;
}

template<typename T>
void LinkedList<T>::PopFront()
{
    if (!head)
        return;
    Node* newHead = head->next;
    delete head;
    head = newHead;
}

template<typename T>
vector<T> ToVector(const LinkedList<T>& list)
{
    vector<T> result;
    for (auto node = list.GetHead(); node; node = node->next) {
        result.push_back(node->value);
    }
    return result;
}