#include <algorithm>
#include <numeric>
#include <vector>
#include <stack>

using namespace std;

template<typename T>
void Swap(T* first, T* second)
{
    swap(*first, *second);
}

template<typename T>
void SortPointers(vector<T*>& pointers)
{
    sort(pointers.begin(), pointers.end(), [](const T* lhv, const T* rhv) {
        return *lhv < *rhv;
    });
}

template<typename T>
void ReversedCopy(T* source, size_t count, T* destination)
{
    stack<T> st;
    for (size_t i = 0; i < count; ++i)
        st.push(*(source + i));
    for (size_t i = 0; i < count; ++i) {
        *(destination + i) = st.top();
        st.pop();
    }
}
