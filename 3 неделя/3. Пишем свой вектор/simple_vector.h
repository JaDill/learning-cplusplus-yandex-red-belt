#pragma once

#include <cstdlib>

// Реализуйте шаблон SimpleVector
template <typename T>
class SimpleVector {
public:
    SimpleVector();
    explicit SimpleVector(size_t size);
    ~SimpleVector();

    T& operator[](size_t index);

    T* begin();
    T* end();

    size_t Size() const;
    size_t Capacity() const;
    void PushBack(const T& value);

private:
    void resize();
    void resize(size_t newCapacity);

    T* _items;
    size_t _size, _capacity;
    // Добавьте поля для хранения данных вектора
};

template<typename T>
SimpleVector<T>::SimpleVector() : _items(nullptr), _size(0), _capacity(0)
{
}

template<typename T>
SimpleVector<T>::SimpleVector(size_t size) : _items(new T[size]), _size(size), _capacity(size)
{
}

template<typename T>
SimpleVector<T>::~SimpleVector()
{
    if (_items)
        delete[] _items;
}

template<typename T>
T& SimpleVector<T>::operator[](size_t index)
{
    return _items[index];
}

template<typename T>
T* SimpleVector<T>::begin()
{
    return _items;
}

template<typename T>
T* SimpleVector<T>::end()
{
    return _items + _size;
}

template<typename T>
size_t SimpleVector<T>::Size() const
{
    return _size;
}

template<typename T>
size_t SimpleVector<T>::Capacity() const
{
    return _capacity;
}

template<typename T>
void SimpleVector<T>::resize()
{
    T* temp = new T[_capacity * 2];
    for (size_t i = 0; i < _size; ++i)
        temp[i] = _items[i];
    delete[] _items;
    _items = temp;
    _capacity *= 2;
}

template<typename T>
void SimpleVector<T>::PushBack(const T& value)
{
    if (_capacity == 0)
        resize(1);
    else if (_size == _capacity)
        resize();
    _items[_size] = value;
    ++_size;
}

template<typename T>
void SimpleVector<T>::resize(size_t newCapacity)
{
    T* temp = new T[newCapacity];
    for (size_t i = 0; i < _size && i < newCapacity; ++i)
        temp[i] = _items[i];
    delete[] _items;
    _items = temp;
    _capacity = newCapacity;
}