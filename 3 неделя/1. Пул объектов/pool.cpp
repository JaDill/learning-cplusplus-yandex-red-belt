#include <algorithm>
#include <iostream>
#include <string>
#include <queue>
#include <stdexcept>
#include <set>

using namespace std;

template<class T>
class ObjectPool
{
public:
    T* Allocate()
    {
        T* result = nullptr;
        if (!deallocated.empty()) {
            result = deallocated.front();
            deallocated.pop();
        } else
            result = new T;
        allocated.insert(result);
        return result;
    }

    T* TryAllocate()
    {
        T* result = nullptr;
        if (!deallocated.empty()) {
            result = deallocated.front();
            deallocated.pop();
            allocated.insert(result);
        }
        return result;
    }

    void Deallocate(T* object)
    {
        auto it = allocated.find(object);
        if (it == allocated.end())
            throw invalid_argument("");
        allocated.erase(it);
        deallocated.push(object);
    }

    ~ObjectPool(){
        for(const auto item : allocated)
            delete item;
        while(!deallocated.empty()) {
            delete deallocated.front();
            deallocated.pop();
        }
    }

private:
    set<T*> allocated;
    queue<T*> deallocated;
};