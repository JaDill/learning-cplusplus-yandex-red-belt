#include "stats.h"

void Stats::AddMethod(string_view method)
{
    auto it = methods.find(method);
    if (it != methods.end())
        ++methods[method];
    else
        ++methods["UNKNOWN"];
}

void Stats::AddUri(string_view uri)
{
    auto it = uris.find(uri);
    if (it != uris.end())
        ++uris[uri];
    else
        ++uris["unknown"];
}

const map<string_view, int>& Stats::GetMethodStats() const
{
    return methods;
}

const map<string_view, int>& Stats::GetUriStats() const
{
    return uris;
}

HttpRequest ParseRequest(string_view line)
{
    while (line.front() == ' ')
        line.remove_prefix(1);
    while(line.back() == ' ')
        line.remove_suffix(1);

    HttpRequest result;
    auto it = line.find(' ');
    result.method = {line.data(), it};
    auto it2 = line.rfind(' ');
    result.uri = {line.data() + it + 1, it2 - it - 1};
    result.protocol = {line.data() + it2 + 1};
    return result;
}
