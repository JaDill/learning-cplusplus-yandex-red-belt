#include <vector>
#include <list>
#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n;
    list<int> head;
    vector<list<int>::const_iterator> q(100'001, head.cend());
    for (int i = 0; i < n; ++i) {
        int who, where;
        cin >> who >> where;
        if (q[where] != head.end())
            q[who] = head.insert(q[where], who);
        else {
            head.push_back(who);
            q[who] = prev(head.cend());
        }
    }
    for (const auto& person : head)
        cout << person << " ";
}


