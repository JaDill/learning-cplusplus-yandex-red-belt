#include <string>
#include <string_view>
#include <deque>
#include <map>

using namespace std;

class Translator
{
public:
    void Add(string_view source, string_view target)
    {
        auto s = getOrCopy(source), t = getOrCopy(target);
        stot[s] = t;
        ttos[t] = s;
    }

    string_view TranslateForward(string_view source) const
    {
        auto item = stot.find(source);
        if (item != stot.end())
            return item->second;
        return {};
    }

    string_view TranslateBackward(string_view target) const
    {
        auto item = ttos.find(target);
        if (item != ttos.end())
            return item->second;
        return {};
    }

private:
    map<string_view, string_view> stot, ttos;
    deque<string> base;

    string_view getOrCopy(string_view s) {
        for (const auto* map_ptr : {&stot, &ttos}) {
            const auto it = map_ptr->find(s);
            if (it != map_ptr->end()) {
                return it->first;
            }
        }
        return base.emplace_back(s);
    }
};