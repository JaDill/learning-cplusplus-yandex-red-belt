#include <string>
#include <list>
//#include "../../1 неделя/test_runner.h"

using namespace std;

class Editor
{
public:
    Editor()
    {
        text.push_back('|');
        cursor_position = text.begin();
    }

    void Left()
    {
        if (cursor_position == text.begin())
            return;
        auto temp = cursor_position;
        --temp;
        swap(*cursor_position, *temp);
        --cursor_position;
    }

    void Right()
    {
        auto temp = cursor_position;
        if (text.end() == ++temp)
            return;
        swap(*cursor_position, *temp);
        ++cursor_position;
    }

    void Insert(char token)
    {
        text.insert(cursor_position, token);
    }

    void Cut(size_t tokens = 1)
    {
        auto begin = cursor_position, end = cursor_position;
        ++begin;
        ++end;
        for (size_t i = 0; i < tokens && end != text.end(); ++i)
            ++end;
        buffer.clear();
        buffer = list<char>(begin, end);

        text.erase(begin, end);
    }

    void Copy(size_t tokens = 1)
    {
        auto begin = cursor_position, end = cursor_position;
        ++begin;
        ++end;
        for (size_t i = 0; i < tokens && end != text.end(); ++i)
            ++end;
        buffer.clear();
        buffer = list<char>(begin, end);
    }

    void Paste()
    {
        for (const auto& item : buffer)
            Insert(item);
    }

    string GetText() const
    {
        string result;
        for (const auto& item : text)
            if (item != '|')
                result.push_back(item);
        return result;
    }

private:
    static const char CURSOR = '|';
    list<char> buffer;
    list<char> text;
    list<char>::iterator cursor_position;
};

//void TypeText(Editor& editor, const string& text)
//{
//    for (char c : text)
//    {
//        editor.Insert(c);
//    }
//}
//
//void TestEditing()
//{
//    {
//        Editor editor;
//
//        const size_t text_len = 12;
//        const size_t first_part_len = 7;
//        TypeText(editor, "hello, world");
//        for (size_t i = 0; i < text_len; ++i)
//        {
//            editor.Left();
//        }
//        editor.Cut(first_part_len);
//        for (size_t i = 0; i < text_len - first_part_len; ++i)
//        {
//            editor.Right();
//        }
//        TypeText(editor, ", ");
//        editor.Paste();
//        editor.Left();
//        editor.Left();
//        editor.Cut(3);
//
//        ASSERT_EQUAL(editor.GetText(), "world, hello");
//    }
//    {
//        Editor editor;
//
//        TypeText(editor, "misprnit");
//        editor.Left();
//        editor.Left();
//        editor.Left();
//        editor.Cut(1);
//        editor.Right();
//        editor.Paste();
//
//        ASSERT_EQUAL(editor.GetText(), "misprint");
//    }
//}
//
//void TestReverse()
//{
//    Editor editor;
//
//    const string text = "esreveR";
//    for (char c : text)
//    {
//        editor.Insert(c);
//        editor.Left();
//    }
//
//    ASSERT_EQUAL(editor.GetText(), "Reverse");
//}
//
//void TestNoText()
//{
//    Editor editor;
//    ASSERT_EQUAL(editor.GetText(), "");
//
//    editor.Left();
//    editor.Left();
//    editor.Right();
//    editor.Right();
//    editor.Copy(0);
//    editor.Cut(0);
//    editor.Paste();
//
//    ASSERT_EQUAL(editor.GetText(), "");
//}
//
//void TestEmptyBuffer()
//{
//    Editor editor;
//
//    editor.Paste();
//    TypeText(editor, "example");
//    editor.Left();
//    editor.Left();
//    editor.Paste();
//    editor.Right();
//    editor.Paste();
//    editor.Copy(0);
//    editor.Paste();
//    editor.Left();
//    editor.Cut(0);
//    editor.Paste();
//
//    ASSERT_EQUAL(editor.GetText(), "example");
//}
//
//int main()
//{
//    TestRunner tr;
//    RUN_TEST(tr, TestEditing);
//    RUN_TEST(tr, TestReverse);
//    RUN_TEST(tr, TestNoText);
//    RUN_TEST(tr, TestEmptyBuffer);
//    return 0;
//}