#pragma once

#include <map>
#include <string>
#include "../profile.h"

using namespace std;

struct Student {
    string first_name;
    string last_name;
    map<string, double> marks;
    double rating;

    bool operator < (const Student& other) const {
        LOG_DURATION("1");
        return GetName() < other.GetName();
    }

    bool Less(const Student& other) const {
        LOG_DURATION("2");
        return rating > other.rating;
    }

    string GetName() const {
        LOG_DURATION("3");
        return first_name + " " + last_name;
    }
};