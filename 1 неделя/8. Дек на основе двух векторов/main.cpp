#include <vector>
#include <stdexcept>

using namespace std;

template<typename T>
class Deque
{
public:
    Deque() = default;

    bool Empty() const
    {
        return vecFront.empty() && vecBack.empty();
    }

    size_t Size() const
    {
        return vecBack.size() + vecFront.size();
    }

    T& operator[](size_t index)
    {
        if (index < vecFront.size())
            return *(vecFront.rbegin() + index);
        return *(vecBack.begin() + (index - vecFront.size()));
    }

    const T& operator[](size_t index) const
    {
        if (index < vecFront.size())
            return *(vecFront.rbegin() + index);
        return *(vecBack.begin() + (index - vecFront.size()));
    }

    T& At(size_t index)
    {
        if (Size() < index)
            throw out_of_range("");
        if (index < vecFront.size())
            return *(vecFront.rbegin() + index);
        return *(vecBack.begin() + (index - vecFront.size()));
    }

    const T& At(size_t index) const
    {
        if (Size() < index)
            throw out_of_range("");
        if (index < vecFront.size())
            return *(vecFront.rbegin() + index);
        return *(vecBack.begin() + (index - vecFront.size()));
    }

    T& Front()
    {
        if (!vecFront.empty())
            return vecFront.back();
        return vecBack.front();
    }

    const T& Front() const
    {
        if (!vecFront.empty())
            return vecFront.back();
        return vecBack.front();
    }

    T& Back()
    {
        if (!vecBack.empty())
            return vecBack.back();
        return vecFront.front();
    }

    const T& Back() const
    {
        if (!vecBack.empty())
            return vecBack.back();
        return vecFront.front();
    }

    void PushFront(const T& value)
    {
        vecFront.push_back(value);
    }

    void PushBack(const T& value)
    {
        vecBack.push_back(value);
    }

private:
    vector<T> vecFront, vecBack;
};