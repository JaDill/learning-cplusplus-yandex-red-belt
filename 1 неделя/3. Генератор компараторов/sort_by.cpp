#include "airline_ticket.h"
#include <tuple>
#include <iostream>

using namespace std;

bool operator<(const Date& lhv, const Date& rhv)
{
    return tie(lhv.year, lhv.month, lhv.day) < tie(rhv.year, rhv.month, rhv.day);
}

bool operator==(const Date& lhv, const Date& rhv)
{
    return tie(lhv.year, lhv.month, lhv.day) == tie(rhv.year, rhv.month, rhv.day);
}

bool operator<(const Time& lhv, const Time& rhv)
{
    return tie(lhv.hours, lhv.minutes) < tie(lhv.hours, lhv.minutes);
}

bool operator==(const Time& lhv, const Time& rhv)
{
    return tie(lhv.hours, lhv.minutes) == tie(lhv.hours, lhv.minutes);
}

ostream& operator<<(ostream& out, const Date& date)
{
    return out << date.year << '-' << date.month << '-' << date.day;
}

ostream& operator<<(ostream& out, const Time& time)
{
    return out << time.hours << ':' << time.minutes;
}

#define SORT_BY(field) \
[](const AirlineTicket& lhv, const AirlineTicket& rhv){ \
    return lhv.field < rhv.field; \
}