#include <utility>
#include<vector>

using namespace std;

template<typename T>
class Table
{
public:
    Table(size_t numRow, size_t numColomn) { Resize(numRow, numColomn); }

    vector<T>& operator[](size_t index)
    {
        return _table[index];
    }

    const vector<T>& operator[](size_t index) const
    {
        return _table[index];
    }

    void Resize(size_t numRow, size_t numColomn)
    {
        _table.resize(numRow);
        for (vector<T>& line : _table)
            line.resize(numColomn);
    }

    pair<size_t, size_t> Size() const
    {
        return {_table.size(), (_table.empty() ? 0u : _table[0].size())};
    }

private:
    vector<vector<T>> _table;
};