#include "airline_ticket.h"
#include <tuple>
#include <iostream>

using namespace std;

bool operator==(const Date& lhv, const Date& rhv)
{
    return tie(lhv.year, lhv.month, lhv.day) == tie(rhv.year, rhv.month, rhv.day);
}

bool operator==(const Time& lhv, const Time& rhv)
{
    return tie(lhv.hours, lhv.minutes) == tie(lhv.hours, lhv.minutes);
}

ostream& operator<<(ostream& out, const Date& date)
{
    return out << date.year << '-' << date.month << '-' << date.day;
}

ostream& operator<<(ostream& out, const Time& time)
{
    return out << time.hours << ':' << time.minutes;
}

istream& operator>>(istream& in, Date& date)
{
    in >> date.year;
    in.ignore(1);
    in >> date.month;
    in.ignore(1);
    in >> date.day;
    return in;
}

istream& operator>>(istream& in, Time& time)
{
    in >> time.hours;
    in.ignore(1);
    in >> time.minutes;
    return in;
}

#define UPDATE_FIELD(ticket, field, values) \
{ \
    auto it = values.find(#field); \
    if(it != values.end()){ \
        istringstream is(it->second); \
        is>>ticket.field; \
    } \
}

